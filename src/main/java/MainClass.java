import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class MainClass {

  public static void main(String args[]) {

      try {

        String fileName = "numbers.txt";

        if (args.length == 0) {
          RandomNumberFileGenerator.generateFileWithRandomNumericValues(fileName);
        } else {
            if (!StringUtils.isNumeric(args[0]) || Integer.parseInt(args[0]) == 0) {
              throw new InvalidInputArgumentException("The first argument of the program has to be a positive integer (file size in MB)");
            }
            int fileSize = Integer.parseInt(args[0]);
            RandomNumberFileGenerator.generateFileWithRandomNumericValues(fileName, fileSize);
        }

        String[] fileContent = GeneratedFileReader.getFileContent(fileName);

        DataAnalysis.analyseContent(fileContent);

      } catch (IOException e) {
        e.printStackTrace();
      } catch (InvalidInputArgumentException e) {
        e.printStackTrace();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }


    }


}
