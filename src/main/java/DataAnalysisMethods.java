public class DataAnalysisMethods {

  public static boolean isArmstrong(String element) {

    long total = 0;

    char[] digits = element.toCharArray();

    for (char digit : digits) {
      total += pow(Character.getNumericValue(digit), digits.length);
    }

    if (total == Long.parseLong(element))
      return true;

    return false;
  }

  private static long pow(long base, long times) {
    long result = 1;
    while (times > 0) {
      result *= base;
      times--;
    }
    return result;
  }
}
