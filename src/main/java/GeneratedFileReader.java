import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class GeneratedFileReader {

  public static String[] getFileContent(String fileName) throws IOException {

    long start = System.currentTimeMillis();

    // Create reader
    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "utf-8"));

    // Read the line and split values
    String[] valueArray = reader.readLine().split(" ");

    reader.close();

    long end = System.currentTimeMillis();

    System.out.println("File reading took " + (end - start) / 1000f + " seconds");
    System.out.println("");

    return valueArray;
  }

}
