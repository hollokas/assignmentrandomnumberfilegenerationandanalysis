import java.util.Map;

public class TenMostFrequentElementsRunnable implements Runnable {

  private Map<String,Integer> map;

  public TenMostFrequentElementsRunnable(Map<String,Integer> map) {
    this.map = map;
  }

  @Override
  public void run() {
    DataAnalysis.displayTenMostFrequentElements(map);
  }
}
