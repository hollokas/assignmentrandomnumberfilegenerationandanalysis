public class InvalidInputArgumentException extends RuntimeException {
  public InvalidInputArgumentException(String message) { super(message); }
}
