import java.math.BigInteger;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

public class DataAnalysis {

  public static void analyseContent(String[] fileContent) throws InterruptedException {

    long start = System.currentTimeMillis();

    // Convert String array to map with array elements as keys and the count of each specific key in the array as values
    final Map<String,Integer> elementFrequencyMap = DataAnalysis.getFileContentMap(fileContent);

    // Use threads to do the analysis
    Thread tenMostFrequentElementsThread = new Thread(new TenMostFrequentElementsRunnable(elementFrequencyMap));
    Thread armstrongAndPrimeNumbersThread = new Thread(new ArmstrongAndPrimeNumbersRunnable(elementFrequencyMap));

    tenMostFrequentElementsThread.start();
    armstrongAndPrimeNumbersThread.start();

    tenMostFrequentElementsThread.join();
    armstrongAndPrimeNumbersThread.join();

    long end = System.currentTimeMillis();

    System.out.println("Data analysis took " + (end - start) / 1000f + " seconds");

  }

  public static void displayArmstrongAndPrimeNumbersCount(Map<String, Integer> elementFrequencyMap) {

    int counterPrime = 0;
    int counterArmstrong = 0;

    for (String element: elementFrequencyMap.keySet()) {

      // Armstrong and prime numbers can't be negative nor decimal numbers -> skip them
      if (element.startsWith("-") || element.contains(".")) {
        continue;
      }

      if (DataAnalysisMethods.isArmstrong(element)) {
        counterArmstrong += elementFrequencyMap.get(element);
      }

      // 1 - (1/2)**certainty => 1 - (1/2)**10 = 99.9% certainty
      if (BigInteger.valueOf(Long.parseLong(element)).isProbablePrime(10)) {
        counterPrime += elementFrequencyMap.get(element);
      }

    }

    System.out.println("Prime numbers count: " + counterPrime);
    System.out.println("Armstrong numbers count: " + counterArmstrong);
    System.out.println("");
  }

  public static void displayTenMostFrequentElements(Map<String, Integer> elementFrequencyMap) {

    // TreeMap with Comparator to sort keys based on values
    Map<String,Integer> orderedElementFrequencyMap = new TreeMap<>(new ElementFrequencyComparator(elementFrequencyMap));

    orderedElementFrequencyMap.putAll(elementFrequencyMap);

    // Take the 10 most frequent keys from the map
    Map<String,Integer> tenMostFrequentElementsMap = orderedElementFrequencyMap.entrySet().stream()
        .limit(10).collect(TreeMap::new, (map, element) -> map.put(element.getKey(), element.getValue()), Map::putAll);


    showBarChart(tenMostFrequentElementsMap);

  }

  private static void showBarChart(Map<String, Integer> tenMostFrequentElementsMap) {

    int longestKey = 0;
    for (String element : tenMostFrequentElementsMap.keySet()) {
      if (element.length() > longestKey) {
        longestKey = element.length();
      }
    }

    System.out.println("Ten Most Frequent Element Bar Chart");

    for (String key: tenMostFrequentElementsMap.keySet()) {

      System.out.print(key + ": ");

      for (int i = 0; i < longestKey - key.length(); i++) {
        System.out.print(" ");
      }

      for(int i = 0; i < tenMostFrequentElementsMap.get(key); i++){
        System.out.print("*");
      }

      System.out.print("\n");

    }

    System.out.println("--------Frequency-------->");
    System.out.println("");

  }

  private static Map<String,Integer> getFileContentMap(String[] fileContent) {

    // ConcurrentHashMap because later the same map is going to be used in different threads
    Map<String,Integer> elementFrequencyMap = new ConcurrentHashMap<>();

    for (String key: fileContent) {
      elementFrequencyMap.merge(key, 1, Integer::sum);
    }

    return elementFrequencyMap;

  }

}
