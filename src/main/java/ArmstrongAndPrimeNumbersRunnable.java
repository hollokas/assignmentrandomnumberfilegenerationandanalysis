import java.util.Map;

public class ArmstrongAndPrimeNumbersRunnable implements Runnable {

  private Map<String, Integer> map;

  public  ArmstrongAndPrimeNumbersRunnable(Map<String, Integer> map) {
    this.map = map;
  }

  @Override
  public void run() {
      DataAnalysis.displayArmstrongAndPrimeNumbersCount(map);
  }
}
