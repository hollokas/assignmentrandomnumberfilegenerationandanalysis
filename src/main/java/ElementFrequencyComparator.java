import java.util.Comparator;
import java.util.Map;

public class ElementFrequencyComparator implements Comparator<String> {

  Map<String,Integer> map;

  public ElementFrequencyComparator(Map<String,Integer> map) {
    this.map = map;

  }

  @Override
  public int compare(String a, String b) {
    if (map.get(a) >= map.get(b)) {
      return -1;
    } else {
      return 1;
    }
  }
}
