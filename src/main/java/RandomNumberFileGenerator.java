import it.unimi.dsi.util.XoRoShiRo128PlusRandom;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class RandomNumberFileGenerator {

  public static void generateFileWithRandomNumericValues(String file) throws IOException{
    generateFileWithRandomNumericValues(file, 64);
  }

  public static void generateFileWithRandomNumericValues(String file, int size) throws IOException{

    long start = System.currentTimeMillis();

    // Define writer
    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));

    // Pseudo random number generator
    XoRoShiRo128PlusRandom random = new XoRoShiRo128PlusRandom();

    // Define maximum file size
    final long MAX_FILE_SIZE = 1024 * 1024 * size;
    int currentTotalSize = 0;


    while (true) {

      // Choose randomly which type of primitive to create
      String randomNumericString = getRandomNumericString(random);

      int sizeOfCurrentString = randomNumericString.getBytes("utf-8").length;

      // Break if adding the new value to the file would make the file size larger than intended
      if (currentTotalSize + sizeOfCurrentString > MAX_FILE_SIZE) {
        break;
      } else {
        writer.write(randomNumericString);
        currentTotalSize += sizeOfCurrentString;
      }

    }

    writer.flush();
    writer.close();

    long end = System.currentTimeMillis();

    System.out.println("File generation took " + (end - start) / 1000f + " seconds");

  }

  private static String getRandomNumericString(XoRoShiRo128PlusRandom random) {
    int tempRandomValue = random.nextInt(4) + 1;
    String tempString = "";

    switch (tempRandomValue) {
      case 1:
        int tempNextInt = random.nextInt();
        tempString = tempNextInt + " ";
        break;

      case 2:
        long tempNextLong = random.nextLong();
        tempString = tempNextLong + " ";
        break;

      case 3:
        float tempNextFloat = random.nextFloat();
        tempString = tempNextFloat + " ";
        break;

      case 4:
        double tempNextDouble = random.nextDouble();
        tempString = tempNextDouble + " ";
        break;

      default:
        break;
    }

    return tempString;
  }
}
